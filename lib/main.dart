import 'package:flutter/material.dart';

import 'package:myapp/decorated_greeting.dart';

const List<Color> gradients = [
  Color.fromARGB(255, 5, 62, 70),
  Color.fromARGB(255, 178, 217, 223)
];

const String message = "Hello World!";

void main() {
  runApp(
    MaterialApp(
      home: Scaffold(body: GradientContainer(message, gradients)),
    ),
  );
}
