import 'package:flutter/material.dart';

import 'package:myapp/dice_roller.dart';

class GradientContainer extends StatelessWidget {
  final String text;
  final List<Color> gradient;

  const GradientContainer(this.text, this.gradient, {super.key});
  const GradientContainer.purple(this.text, {super.key})
      : gradient = const [
          Color.fromARGB(255, 5, 62, 70),
          Color.fromARGB(255, 178, 217, 223)
        ];

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        gradient: LinearGradient(
          colors:
              gradient, //[Color.fromARGB(255, 5, 62, 70), Color.fromARGB(255, 178, 217, 223) ],
          begin: Alignment.topLeft,
          end: Alignment.bottomRight,
        ),
      ),
      child: const Center(
        child: DiceRoller() 
      ),
    );
  }
}
